# Page 1

{% code title="test.lib" overflow="wrap" %}
```py
    $ ppl-token create-account AQoKYV7tYpTrFZN6P5oUufbQKAUr9mNYGe1TTJC9wajM
    Creating account 7UX2i7SucgLMQcfZ75s3VXmZZY4YRUyJN9X1RgfMoDUi
    Signature: 42Sa5eK9dMEQyvD9GMHuKxXf55WLZ7tfjabUKDhNoZRAxj9MsnN7omriWMEHXLea3aYpjZ862qocRLVikvkHkyfy
```
{% endcode %}

```python
$$
\begin{aligned}
	D_{us} &= \left( \frac{P_{us}(t_{1}) - P_{us}(t_{0})}{P_{us}(t_{0})} \right)\\
		&= \left( \frac{ \left( \frac{SOL_{us}(t_{2})}{SOL_{total}(t_{2})} \right) - \left( \frac{SOL_{us}(t_{1})}{SOL_{total}(t_{1})} \right)}{ \left( \frac{SOL_{us}(t_{1})}{SOL_{total}(t_{1})} \right) } \right)\\

\end{aligned}
$$
```



<figure><img src=".gitbook/assets/QQ图片20230217172925.jpg" alt=""><figcaption></figcaption></figure>

$$
\begin{aligned} D_{us} &= \left( \frac{P_{us}(t_{1}) - P_{us}(t_{0})}{P_{us}(t_{0})} \right)\\ &= \left( \frac{ \left( \frac{SOL_{us}(t_{2})}{SOL_{total}(t_{2})} \right) - \left( \frac{SOL_{us}(t_{1})}{SOL_{total}(t_{1})} \right)}{ \left( \frac{SOL_{us}(t_{1})}{SOL_{total}(t_{1})} \right) } \right)\\ \end{aligned}
$$

```
$$
\begin{aligned}
	SOL_{us}(t_2) &= SOL_{us}(t_1)\\
	SOL_{total}(t_2) &= SOL_{total}(t_1)\times (1 + I_{t_1})\\
\end{aligned}
$$
```



$$
\begin{aligned} SOL_{us}(t_2) &= SOL_{us}(t_1)\\ SOL_{total}(t_2) &= SOL_{total}(t_1)\times (1 + I_{t_1})\\ \end{aligned}
$$

```
$$
\begin{aligned}
	D_{us} &= \left( \frac{ \left( \frac{SOL_{us}(t_{1})}{SOL_{total}(t_{1})\times (1 + I_{1})} \right) - \left( \frac{SOL_{us}(t_{1})}{SOL_{total}(t_{1})} \right)}{ \left( \frac{SOL_{us}(t_{1})}{SOL_{total}(t_{1})} \right) } \right)\\
	D_{us} &= \frac{1}{(1 + I_{1})} - 1\\
\end{aligned}
$$
```



$$
\begin{aligned} D_{us} &= \left( \frac{ \left( \frac{SOL_{us}(t_{1})}{SOL_{total}(t_{1})\times (1 + I_{1})} \right) - \left( \frac{SOL_{us}(t_{1})}{SOL_{total}(t_{1})} \right)}{ \left( \frac{SOL_{us}(t_{1})}{SOL_{total}(t_{1})} \right) } \right)\\ D_{us} &= \frac{1}{(1 + I_{1})} - 1\\ \end{aligned}
$$



```
$$
\begin{aligned}
Y_{adj} &=  \frac{ 1 + I/P_s }{ 1 + I } - 1,~\text{and}\\
D_{us} &= -\frac{I}{I + 1},~\text{so} \\
\frac{D_{us}}{Y_{adj}} &= \frac{ \frac{I}{I + 1} }{ \frac{ 1 + I/P_s }{ 1 + I } - 1 } \\
\end{aligned}
$$
```



$$
\begin{aligned} Y_{adj} &= \frac{ 1 + I/P_s }{ 1 + I } - 1,~\text{and}\\ D_{us} &= -\frac{I}{I + 1},~\text{so} \\ \frac{D_{us}}{Y_{adj}} &= \frac{ \frac{I}{I + 1} }{ \frac{ 1 + I/P_s }{ 1 + I } - 1 } \\ \end{aligned}
$$







1. (De)Serialization with type info · Issue #1095 · serde-rs/serde
2. `std::any::type_name` - Rust
3. Parity's ink to write smart contracts

* dffdfdf
* fggfgf
* gffggfgfgfgf



* [ ] gfghhg
* [ ] ghghhhhhhhhhhhhhhghghg
* [ ] jhhhhhhhhhhhhhhhhhhjhjhjh





<table data-view="cards"><thead><tr><th></th><th></th><th></th></tr></thead><tbody><tr><td>1112</td><td></td><td></td></tr><tr><td></td><td>122121</td><td></td></tr><tr><td></td><td>12221</td><td></td></tr><tr><td></td><td>2132323232</td><td></td></tr><tr><td>3233232</td><td></td><td></td></tr><tr><td>ewweweewew</td><td></td><td></td></tr><tr><td></td><td></td><td></td></tr></tbody></table>



<details>

<summary>weweew</summary>

er

erer

er

</details>



$$
f(x) = x * e^{2 pi i \xi x}
$$





```
123232
433444545
```

```xojo
enum LockedPubkey {
    Read(Pubkey),
    Write(Pubkey),
}
```

> dffd
>
> dfddfdfd
>
>
>
> fddddddddddddddddddffggffg
>
> * gffggfgffg
> * fg
> * fggf
> * gfgfgfgffgfggf

* Testnet may be subject to ledger resets.
  * Testnet includes a token faucet for airdrops for application testing
    * Testnet typically runs a newer software release branch than both Devnet and Mainnet Beta
    * Gossip entrypoint for Testnet: `entrypoint.testnet.solana.com:8001`
  * Metrics environment variable for Testnet:
    1. cvcvvvvvvv
    2. cvcvvvvvvv
  * Metrics environment variable for Testnet:
* Metrics environment variable for Testnet:
* Metrics environment variable for Testnet:

<figure><img src=".gitbook/assets/icon.svg" alt=""><figcaption></figcaption></figure>

<img src=".gitbook/assets/file.drawing.svg" alt="" class="gitbook-drawing">

123333

```bash
export SOLANA_METRICS_CONFIG="host=https://metrics.solana.com:8086,db=tds,u=testnet_write,p=c4fa841aa918bf8274e3e2a44d77568d9861b3ea"
```

vcccccccccccc









### Testnet

* Testnet is where the Solana core contributors stress test recent release features on a live cluster, particularly focused on network performance, stability and validator behavior.
* Testnet tokens are **not real**

cvcvvvvvvv cvcvvvvvvv

* Testnet may be subject to ledger resets.
* Testnet includes a token faucet for airdrops for application testing
* Testnet typically runs a newer software release branch than both Devnet and Mainnet Beta
* Gossip entrypoint for Testnet: `entrypoint.testnet.solana.com:8001`
* Metrics environment variable for Testnet:

1. cvcvvvvvvv
2. cvcvvvvvvv
3. dsdssddddddddddddsd

```bash
export SOLANA_METRICS_CONFIG="host=https://metrics.solana.com:8086,db=tds,u=testnet_write,p=c4fa841aa918bf8274e3e2a44d77568d9861b3ea"
```

vcccccccccccc vcccccccccccc vcccccccccccc

* RPC URL for Testnet: `https://api.testnet.solana.com`







