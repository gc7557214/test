# Adjusted Staking Yield

#### Token Dilution[#](https://docs.solana.com/inflation/adjusted\_staking\_yield#token-dilution)

Similarly we can look at the expected _Staked Dilution_ (i.e. _Adjusted Staking Yield_) and _Un-staked Dilution_ as previously defined. Again, _dilution_ in this context is defined as the change in fractional representation (i.e. ownership) of a set of tokens within a larger set. In this sense, dilution can be a positive value: an increase in fractional ownership (staked dilution / _Adjusted Staking Yield_), or a negative value: a decrease in fractional ownership (un-staked dilution).

We are interested in the relative change in ownership of staked vs un-staked tokens as the overall token pool increases with inflation issuance. As discussed, this issuance is distributed only to staked token holders, increasing the staked token fractional representation of the _Total Current Supply_.

Continuing with the same _Inflation Schedule_ parameters as above, we see the fraction of staked supply grow as shown below.

![](https://docs.solana.com/assets/images/p\_ex\_staked\_supply\_w\_range\_initial\_stake-4b30afe37638c22b2b17a89ed2c8ebbe.png)

Due to this relative change in representation, the proportion of stake of any token holder will also change as a function of the _Inflation Schedule_ and the proportion of all tokens that are staked.

Of initial interest, however, is the _dilution of **un-staked** tokens_, or $$DusDus​$$. In the case of un-staked tokens, token dilution is only a function of the _Inflation Schedule_ because the amount of un-staked tokens doesn't change over time.

This can be seen by explicitly calculating un-staked dilution as $$DusDus​$$. The un-staked proportion of the token pool at time $$tt$$ is $$Pus(tN)Pus​(tN​)$$ and $$ItIt​$$ is the incremental inflation rate applied between any two consecutive time points. $$SOLus(t)SOLus​(t)$$ and $$SOLtotal(t)SOLtotal​(t)$$ is the amount of un-staked and total SOL on the network, respectively, at time $$tt$$. Therefore $$Pus(t)=SOLus(t)/SOLtotal(t)Pus​(t)=SOLus​(t)/SOLtotal​(t)$$.

$$
Dus=(Pus(t1)−Pus(t0)Pus(t0))=((SOLus(t2)SOLtotal(t2))−(SOLus(t1)SOLtotal(t1))(SOLus(t1)SOLtotal(t1)))Dus​​=(Pus​(t0​)Pus​(t1​)−Pus​(t0​)​)=⎝
$$

⎛​(SOLtotal​(t1​)SOLus​(t1​)​)(SOLtotal​(t2​)SOLus​(t2​)​)−(SOLtotal​(t1​)SOLus​(t1​)​)​⎠

$$
⎞​​
$$

However, because inflation issuance only increases the total amount and the un-staked supply doesn't change:

$$
SOLus(t2)=SOLus(t1)SOLtotal(t2)=SOLtotal(t1)×(1+It1)SOLus​(t2​)SOLtotal​(t2​)​=SOLus​(t1​)=SOLtotal​(t1​)×(1+It1​​)​
$$

So $$DusDus​$$ becomes:

$$
Dus=((SOLus(t1)SOLtotal(t1)×(1+I1))−(SOLus(t1)SOLtotal(t1))(SOLus(t1)SOLtotal(t1)))Dus=1(1+I1)−1Dus​Dus​​=⎝
$$

⎛​(SOLtotal​(t1​)SOLus​(t1​)​)(SOLtotal​(t1​)×(1+I1​)SOLus​(t1​)​)−(SOLtotal​(t1​)SOLus​(t1​)​)​⎠

$$
⎞​=(1+I1​)1​−1​
$$

Or generally, dilution for un-staked tokens over any time frame undergoing inflation $$II$$:

$$
Dus=−II+1Dus​=−I+1I​
$$

So as guessed, this dilution is independent of the total proportion of staked tokens and only depends on inflation rate. This can be seen with our example _Inflation Schedule_ here:

![p\_ex\_unstaked\_dilution](https://docs.solana.com/assets/images/p\_ex\_unstaked\_dilution-c3bcfaa5aa940c3f3e877e4e64710026.png)

#### Estimated Adjusted Staked Yield[#](https://docs.solana.com/inflation/adjusted\_staking\_yield#estimated-adjusted-staked-yield) <a href="#estimated-adjusted-staked-yield" id="estimated-adjusted-staked-yield"></a>

We can do a similar calculation to determine the _dilution_ of staked token holders, or as we've defined here as the _**Adjusted Staked Yield**_, keeping in mind that dilution in this context is an _increase_ in proportional ownership over time. We'll use the terminology _Adjusted Staked Yield_ to avoid confusion going forward.

To see the functional form, we calculate, $$YadjYadj​$$, or the _Adjusted Staked Yield_ (to be compared to _D\_{us}_ the dilution of un-staked tokens above), where $$Ps(t)Ps​(t)$$ is the staked proportion of token pool at time $$tt$$ and $$ItIt​$$ is the incremental inflation rate applied between any two consecutive time points. The definition of $$YadjYadj​$$ is therefore:

$$
Yadj=Ps(t2)−Ps(t1)Ps(t1)Yadj​=Ps​(t1​)Ps​(t2​)−Ps​(t1​)​
$$

As seen in the plot above, the proportion of staked tokens increases with inflation issuance. Letting $$SOLs(t)SOLs​(t)$$ and $$SOLtotal(t)SOLtotal​(t)$$ represent the amount of staked and total SOL at time $$tt$$ respectively:

$$
Ps(t2)=SOLs(t1)+SOLtotal(t1)×I(t1)SOLtotal(t1)×(1+I(t1))Ps​(t2​)=SOLtotal​(t1​)×(1+I(t1​))SOLs​(t1​)+SOLtotal​(t1​)×I(t1​)​
$$

Where $$SOLtotal(t1)×I(t1)SOLtotal​(t1​)×I(t1​)$$ is the additional inflation issuance added to the staked token pool. Now we can write $$YadjYadj​$$ in common terms $$t1=tt1​=t$$:

$$
Yadj=SOLs(t)+SOLtotal(t)×I(t)SOLtotal(t)×(1+I(t))−SOLs(t)SOLtotal(t)SOLs(t)SOLtotal(t)=SOLtotal(t)×(SOLs(t)+SOLtotal(t)×I(t))SOLs(t)×SOLtotal×(1+I(t))−1Yadj​​=SOLtotal​(t)SOLs​(t)​SOLtotal​(t)×(1+I(t))SOLs​(t)+SOLtotal​(t)×I(t)​−SOLtotal​(t)SOLs​(t)​​=SOLs​(t)×SOLtotal​×(1+I(t))SOLtotal​(t)×(SOLs​(t)+SOLtotal​(t)×I(t))​−1​
$$

which simplifies to:

$$
Yadj=1+I(t)/Ps(t)1+I(t)−1Yadj​=1+I(t)1+I(t)/Ps​(t)​−1
$$

So we see that the _Adjusted Staked Yield_ a function of the inflation rate and the percent of staked tokens on the network. We can see this plotted for various staking fractions here:

![p\_ex\_adjusted\_staked\_yields](https://docs.solana.com/assets/images/p\_ex\_adjusted\_staked\_yields-27926d55c81933fabb256c7a8b0e1e8a.png)

It is also clear that in all cases, dilution of un-staked tokens $$>>$$ adjusted staked yield (i.e. dilution of staked tokens). Explicitly we can look at the _relative dilution of un-staked tokens to staked tokens:_ $$Dus/YadjDus​/Yadj​$$. Here the relationship to inflation drops out and the relative dilution, i.e. the impact of staking tokens vs not staking tokens, is purely a function of the % of the total token supply staked. From above

$$
Yadj=1+I/Ps1+I−1, andDus=−II+1, soDusYadj=II+11+I/Ps1+I−1Yadj​Dus​Yadj​Dus​​​=1+I1+I/Ps​​−1, and=−I+1I​, so=1+I1+I/Ps​​−1I+1I​​​
$$

which simplifies as,

$$
DusYadj=I1+IPs−(1+I)=IIPs−IDusYadj=Ps1−PsYadj​Dus​​Yadj​Dus​​​=1+Ps​I​−(1+I)I​=Ps​I​−II​=1−Ps​Ps​​​
$$

Where we can see a primary dependence of the relative dilution of un-staked tokens to staked tokens is on the function of the proportion of total tokens staked. As shown above, the proportion of total tokens staked changes over time (i.e. $$Ps=Ps(t)Ps​=Ps​(t)$$ due to the re-staking of inflation issuance thus we see relative dilution grow over time as:

![p\_ex\_relative\_dilution](https://docs.solana.com/assets/images/p\_ex\_relative\_dilution-559da040e2b2f2919f0dc416ab4e7136.png)

As might be intuitive, as the total fraction of staked tokens increases the relative dilution of un-staked tokens grows dramatically. E.g. with $$80%80%$$ of the network tokens staked, an un-staked token holder will experience \~$$400%400%$$ more dilution than a staked holder.

Again, this represents the change in fractional change in ownership of staked tokens and illustrates the built-in incentive for token holder to stake their tokens to earn _Staked Yield_ and avoid _Un-staked Dilution_.
